/*
Autor: Gabriela Pacakova
Login: xpacak01
Datum: Apr 18, 2016
Popis: Cielom programu je implementacia synchronizacneho problemu Roller Coaster pomocou semaforov.
*/

#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#include <string.h>
#include <ctype.h>


#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>


#define SH_MEM 1
#define FORK 2
#define PARAMETER_ERROR 3
#define FOPEN_ERROR 4
#define SEM_MAKE 5
#define SEM_DEL 6
#define SEM_WORK 7


/****Structures*****/
typedef struct{
	int p; //number of passengers
	int c; //capacity of the car
	int pt; //max passenger generation time
	int rt; //max car ride time
} Arguments;

typedef struct {
	int action_id; //number of the action performed
	int boardedPass; //number of just boarded passengers
	int riddenPass; //number of passengers that completed the ride
	int unboardedPass; //number of just unboarded passengers
} Shared;

/*Global variables*/
const int car = 1; // Just 1 car is required for this solution

pid_t pid_passgen; //process for passenger generation
pid_t pid_car; //car process

/*Semaphores*/

sem_t *xpacak01_ACCESS;
sem_t *xpacak01_LOAD;
sem_t *xpacak01_BOARD_QUE;
sem_t *xpacak01_UNBOARD_QUE;
sem_t *xpacak01_ALL_ABOARD;
sem_t *xpacak01_ALL_ASHORE;

FILE *file_out=NULL; // output file pointer

Arguments *args; //structure for arguments
Shared *Memstruct; //structure for shared variables
int shm_args, shm_memoire;

/*Function prototypes*/

int car_fc(); //handles car behaviour
int passenger_fc(int pass_id); //handles passenger behaviour
void run(); //handles car ride
void parent_handler();//handles parent process
void car_handler();//handles car process
void pass_handler();//handles passenger process
void pass_gen_handler();//handles passenger generator process
int semaphores();//handles semaphore and shared memory initialization

void process_arg(int argc, char *argv[]);//handles argument processing

void errorito(int number);//handles output to stderr

void close_semaphores();//handles semaphore closing
void shutdown();//handles safe shutdown of the program

void haalp();//handles help output, arg -h

/******* let the MAIN begin ********/

int main(int argc, char *argv[])
{
	signal(SIGINT, parent_handler);
	signal(SIGKILL, parent_handler);

/******LOAD SEMS AND SHM*********/
	if(semaphores()!=0) //if semaphores and shared memory isn't correctly initialised, exits with error
	{
		errorito(SH_MEM);
		shutdown();
		exit(2);
	}

/******HELP*****/
	if ((argc == 2) && (strcmp("-h", argv[1]) == 0)) //displays help with -h argument
	{
		haalp();
		shutdown(); 
		exit(EXIT_SUCCESS);
	}

	process_arg(argc,argv); //processes arguments

/******OUTPUT FILE******/

	file_out = fopen("proj2.out", "w+");

	if(file_out == NULL)
	{
		file_out = fopen("proj2.out", "wb");
		if(file_out == NULL)
		{
			errorito(FOPEN_ERROR);
		}
	}
	setbuf(file_out, NULL);

/******PROCESS CREATION******/
/**Process generating passengers****/

	pid_passgen = fork();

	if(pid_passgen >= 0) //if fork didn't fail
	{
		if(pid_passgen == 0)
		{
			//SIG handlers for args->p generator
			signal(SIGTERM, pass_gen_handler);
			signal(SIGINT, pass_gen_handler);

			//create passengers according to argv[1]
			for(int i = 1; i < (args->p + 1); i++)
			{
				//usleep(rand() % (args->rt-0)+1);
				//delay
				int delay = 1000*(rand() % (args->pt+1));

				if (args->pt == 0)
					usleep(1);
				else
					usleep(delay);

				pid_t pid_pass = fork();

				if(pid_pass >=0)
				{
					if(pid_pass == 0)
					{
						signal(SIGTERM, pass_handler);
						signal(SIGINT, pass_handler);

						passenger_fc(i);//calls function for passenger behaviour, i= passenger number

						//closes all semaphores on finish
						close_semaphores();
						exit(0);
					}
				}
				else //fork failed, passenger not created
				{
					errorito(FORK);

					//KILL THE CHILDREN
					kill(0,SIGSTOP);
					kill(0,SIGTERM);
					exit(2);
				}
			}
			//wait for the children
			for(int i = 1; i < ((args->p) + 1); i++)
			{
				wait(NULL);
			}
			exit(0);
		}
		else //code for parent
		{
			// parent
		}
	}

	else // fork failed and pass_gen process not created
	{
		errorito(FORK);
		close_semaphores();
		return 2;
	}

/*******CAR PROCESS*******/
	pid_car = fork();

	if(pid_car >=0) //if fork was successful
	{
		if(pid_car == 0)
		{
			signal(SIGTERM, car_handler);
			signal(SIGINT, car_handler);

			car_fc();//calls function for car behaviour

			close_semaphores();
			exit(0);
		}

		wait(NULL);
	}
	else //fork wasn't successful, car process not created
	{
		errorito(FORK);
		kill(0,SIGSTOP);
		kill(0,SIGTERM);
		exit(2);
	}
	wait(NULL);
	shutdown(); //exiting the program
	return 0;
}
/********MAIN: The End********/

/*******SEMAPHORES & SHM***********/
int semaphores()
{
	/********SEMAPHORES*******/
	xpacak01_ACCESS = sem_open("xpacak01_ACCESS", O_CREAT | O_EXCL, 0644, 1);
	xpacak01_LOAD = sem_open("xpacak01_LOAD", O_CREAT | O_EXCL, 0644, 0);
	xpacak01_BOARD_QUE = sem_open("xpacak01_BOARD_QUE", O_CREAT | O_EXCL, 0644, 0);
	xpacak01_UNBOARD_QUE = sem_open("xpacak01_UNBOARD_QUE", O_CREAT | O_EXCL, 0644, 0);
	xpacak01_ALL_ABOARD = sem_open("xpacak01_ALL_ABOARD", O_CREAT | O_EXCL, 0644, 0);
	xpacak01_ALL_ASHORE = sem_open("xpacak01_ALL_ASHORE", O_CREAT | O_EXCL, 0644, 0);

	if(xpacak01_ACCESS == SEM_FAILED){errorito(SEM_MAKE);}
	if(xpacak01_LOAD == SEM_FAILED){errorito(SEM_MAKE);}
	if(xpacak01_BOARD_QUE == SEM_FAILED){errorito(SEM_MAKE);}
	if(xpacak01_UNBOARD_QUE == SEM_FAILED){errorito(SEM_MAKE);}
	if(xpacak01_ALL_ABOARD == SEM_FAILED){errorito(SEM_MAKE);}
	if(xpacak01_ALL_ASHORE == SEM_FAILED){errorito(SEM_MAKE);}

	/*********SHARED MEMORY**********/

	shm_memoire = shm_open("xpacak01_sharedMemspace", O_CREAT | O_EXCL | O_RDWR, 0644);
	shm_args = shm_open("xpacak01_sharedArgs", O_CREAT | O_EXCL | O_RDWR, 0644);
	if(shm_memoire == -1){errorito(SH_MEM);}
	if(shm_args == -1){errorito(SH_MEM);}
	ftruncate(shm_memoire, sizeof(Shared));
	ftruncate(shm_args, sizeof(Arguments));

	Memstruct = mmap(NULL, sizeof(Shared), PROT_READ | PROT_WRITE, MAP_SHARED, shm_memoire, 0);
	Memstruct->action_id = 1;
	Memstruct->boardedPass = 0;
	Memstruct->riddenPass = 0;
	Memstruct->unboardedPass = 0;

	args = mmap(NULL, sizeof(Arguments), PROT_READ | PROT_WRITE, MAP_SHARED, shm_args, 0);
	args->p = 0;
	args->c = 0;
	args->pt = 0;
	args->rt = 0;

	return 0;
}

/******CAR********/
int car_fc()
{
	//number of passengers / capacity of the car = number of rides that need to be performed
	int rides_no = (args->p) / (args->c);
	
	/******Starts the car*******/
	//every action waits for access, so it can use shared resources => prevents collision
	sem_wait(xpacak01_ACCESS);

	fprintf(file_out,"%d: C %d: started\n", Memstruct->action_id, car);
	(Memstruct->action_id)++; //every action increments the action number

	//every action gives up access to shared resources, so other actions can use it
	sem_post(xpacak01_ACCESS);

	/******Starts the rides*****/
	for(int a=rides_no;a>0;a--)
	{
			
		sem_wait(xpacak01_ACCESS);
		//preparing to load passengers
		fprintf(file_out,"%d: C %d: load\n", Memstruct->action_id, car);
		(Memstruct->action_id)++;
		sem_post(xpacak01_ACCESS);

		sem_post(xpacak01_BOARD_QUE);//gives signal to passengers that they can board
	
		sem_wait(xpacak01_ALL_ABOARD);//waits for signal until all passengers boarded
	
		sem_wait(xpacak01_ACCESS); //preparing to run the car
		fprintf(file_out,"%d: C %d: run\n", Memstruct->action_id, car);
		(Memstruct->action_id)++;
		sem_post(xpacak01_ACCESS);

		sem_wait(xpacak01_ACCESS);
		run(); //car runs, no one can board or unboard
		sem_post(xpacak01_ACCESS);

		sem_wait(xpacak01_ACCESS); //preparing to unload passengers
		fprintf(file_out,"%d: C %d: unload\n", Memstruct->action_id, car);
		(Memstruct->action_id)++;
		sem_post(xpacak01_UNBOARD_QUE);//gives signal to passengers that they can unload
		sem_post(xpacak01_ACCESS);
	
		sem_wait(xpacak01_ALL_ASHORE);//waits for signal until all pass are ashore, then repeats the loop
	}

	//finishes after every passenger has completed the ride
	sem_wait(xpacak01_LOAD);
	sem_wait(xpacak01_ACCESS);
	fprintf(file_out,"%d: C %d: finished\n", Memstruct->action_id, car);
	(Memstruct->action_id)++;
	sem_post(xpacak01_ACCESS);
	

	return 0;
}

/*****PASSENGER*****/
int passenger_fc(int pass_id)
{
	
	sem_wait(xpacak01_ACCESS);
	//initiates the passengers
	fprintf(file_out,"%d: P %d: started\n", Memstruct->action_id, pass_id);
	(Memstruct->action_id)++;
	sem_post(xpacak01_ACCESS);

	sem_wait(xpacak01_BOARD_QUE);//waits for signal to board

	sem_wait(xpacak01_ACCESS);
	fprintf(file_out,"%d: P %d: board\n", Memstruct->action_id, pass_id);
	(Memstruct->action_id)++;
	Memstruct->boardedPass += 1;//increments the boarded passengers number
	if(Memstruct->boardedPass<args->c)//if there is still space in the car, lists the passenger board order
	{
		fprintf(file_out,"%d: P %d: board order %d\n", Memstruct->action_id, pass_id, Memstruct->boardedPass);
		(Memstruct->action_id)++;
		sem_post(xpacak01_BOARD_QUE);
	}
	else if(Memstruct->boardedPass == args->c)//if the passenger boarding is the last one that fits
	{
		fprintf(file_out,"%d: P %d: board last\n", Memstruct->action_id, pass_id);
		(Memstruct->action_id)++;
		Memstruct->boardedPass = 0;//sets boarded passengers to 0, to prepare for the next ride
		
		sem_post(xpacak01_ALL_ABOARD);//signals the car to run	
		
	}
	
	sem_post(xpacak01_ACCESS);

	//waits for signal until the ride is finished, then starts to unboard
	sem_wait(xpacak01_UNBOARD_QUE);
	
	sem_wait(xpacak01_ACCESS);
	//unboarding	
	Memstruct->unboardedPass += 1;//increments the no of unboarded passengers
	fprintf(file_out,"%d: P %d: unboard\n", Memstruct->action_id, pass_id);
	
	(Memstruct->action_id)++;
	sem_post(xpacak01_ACCESS);

	sem_wait(xpacak01_ACCESS);	
	if(Memstruct->unboardedPass<args->c)//if it's not the last passenger to unboard
	{
		fprintf(file_out,"%d: P %d: unboard order %d\n", Memstruct->action_id, pass_id, Memstruct->unboardedPass);
		(Memstruct->action_id)++;
		sem_post(xpacak01_UNBOARD_QUE);
	}	
	else if(Memstruct->unboardedPass == args->c)//if it's the last passenger to unboard
	{
		fprintf(file_out,"%d: P %d: unboard last\n", Memstruct->action_id, pass_id);
		(Memstruct->action_id)++;
		Memstruct->unboardedPass = 0;//sets unboarded to 0 for next unboarding
		sem_post(xpacak01_ALL_ASHORE);//signals the car that it can start to load new passengers	
	}
	
	sem_post(xpacak01_ACCESS);

	sem_wait(xpacak01_ACCESS);
	Memstruct->riddenPass+=1;//increments for every passenger that completed the ride
	if(Memstruct->riddenPass == (args->p))//if it's the same as initial number of passengers
	{
		for(int k=0;k<args->p;k++)
		{
			sem_post(xpacak01_LOAD);//posts signal that all passengers can finish p times
		}	
	}
	sem_post(xpacak01_ACCESS);
	
	//waits for signal that all passengers can finish and prints it out
	sem_wait(xpacak01_LOAD);

	sem_wait(xpacak01_ACCESS);
	fprintf(file_out,"%d: P %d: finished\n", Memstruct->action_id, pass_id);
	(Memstruct->action_id)++;
	sem_post(xpacak01_LOAD);
	sem_post(xpacak01_ACCESS);
	

	return 0;
}

/*****WEHEEEEY***********/

void run() //waits for random value between 0 and rt = the car is running
{
	//usleep(rand() % (args->rt-0)+1);
	int delay = 1000*(rand() % (args->rt+1));

				if (args->rt == 0)
					usleep(1);
				else
					usleep(delay);
}


/******ARGUMENT HANDLING********/
void process_arg(int argc, char *argv[])
{
	if (argc != 5) //checks the argument count
	{
		errorito(PARAMETER_ERROR);
	}
	else //checks if the arguments are in the correct format and range
	{
		if(isdigit(*argv[1]))
		{
			args->p=atoi(argv[1]);
			if (args->p<0) {errorito(PARAMETER_ERROR);}
		} else{errorito(PARAMETER_ERROR);}

		if(isdigit(*argv[2]))
		{
			args->c=atoi(argv[2]);
			if ((args->c<0) || (args->c>args->p) || (args->p%args->c!=0)) {errorito(PARAMETER_ERROR);}
		} else{errorito(PARAMETER_ERROR);}

		if(isdigit(*argv[3]))
		{
			args->pt=atoi(argv[3]);
			if ((args->pt<0) || (args->pt>5001)) {errorito(PARAMETER_ERROR);}
		} else{errorito(PARAMETER_ERROR);}

		if(isdigit(*argv[4]))
		{
			args->rt=atoi(argv[4]);
			if ((args->rt<0) || (args->rt>5001)) {errorito(PARAMETER_ERROR);}
		} else{errorito(PARAMETER_ERROR);}
	}
}

/*********ERROR HANDLING**********/

void errorito(int number)
{
	switch (number)
	{
		case SH_MEM:
			fprintf(stderr, "ERROR: An error occured while accessing shared memory.\n");
			break;
		case FORK:
			fprintf(stderr, "ERROR: An error occured while creating a process.\n");
			break;
		case PARAMETER_ERROR:
			fprintf(stderr, "ERROR: Incorrect given parameters.\n");
			break;
		case FOPEN_ERROR:
			fprintf(stderr, "ERROR: An error occured opening or creating output file.\n");
			break;
		case SEM_MAKE:
			fprintf(stderr, "ERROR: An error occured while initialising a semaphore.\n");
			break;
		case SEM_DEL:
			fprintf(stderr, "ERROR: An error occured while deleting a semaphore.\n");
			break;
		case SEM_WORK:
			fprintf(stderr, "ERROR: An error occured while using a semaphore.\n");
			break;
		default:
			fprintf(stdout, "ERROR: Runtime error.\n");
	}
	fprintf(stderr, "To dislpay help, please use ./proj2 -h.\n");
	close_semaphores();
	shutdown();
	exit(2);
}

void close_semaphores() //closes semaphores
{
	sem_close(xpacak01_ACCESS);
	sem_close(xpacak01_LOAD);
	sem_close(xpacak01_BOARD_QUE);
	sem_close(xpacak01_UNBOARD_QUE);
	sem_close(xpacak01_ALL_ABOARD);
	sem_close(xpacak01_ALL_ASHORE);
}

void shutdown() //unlinks shared memory and output file
{
	sem_unlink("xpacak01_ACCESS");
	sem_unlink("xpacak01_LOAD");
	sem_unlink("xpacak01_BOARD_QUE");
	sem_unlink("xpacak01_UNBOARD_QUE");
	sem_unlink("xpacak01_ALL_ABOARD");
	sem_unlink("xpacak01_ALL_ASHORE");

	munmap(Memstruct, sizeof(int));
	shm_unlink("xpacak01_sharedMemspace");
	shm_unlink("xpacak01_sharedArgs");

	close(shm_memoire);
	close(shm_args);

	if(file_out != NULL)
		fclose(file_out);

}

/***PROCESS HANDLING FCS****/
//they kill processes and make the program shut down correctly
//
void parent_handler()
{
		kill(0,SIGTERM);
		shutdown();
		exit(0);
}	

void pass_gen_handler()
{
		kill(0,SIGTERM);
		parent_handler();
		exit(0);
}

void car_handler()
{
		parent_handler();
		close_semaphores();
		exit(0);
}

void pass_handler()
{
	parent_handler();
	close_semaphores();
	exit(0);
}

//help message
void haalp()
{
	printf("Autor: Gabriela Pacakova\n"
			"Datum vytvorenia: Apr 18, 2016\n"
			"Program: Cielom programu je implementacia synchronizacneho\n"
			"\t problemu Roller Coaster pomocou semaforov.\n"
			"Spustenie: ./proj2 4 2 0 0\n"
			"\t Spusti Roller Coaster so 4 pasaziermi, kapacitou vozika 2 pasazieri,\n"
			"\t max doba generacie dalsieho pasaziera 0 ms, max cas prejazdu 0 ms.\n"
			"Poznamka: Pocet pasazierov musi byt nasobkom kapacity vozika.\n");
}