Autor: Gabriela Pacakova
Login: xpacak01
Datum: Apr 18, 2016
Popis: Cielom programu je implementacia synchronizacneho problemu Roller Coaster pomocou semaforov.
Hodnotenie: 14/15b 

make proj2
./proj2 -h (zobrazeni napovedi)

Pozn.: Nasledujuce zdrojove kody sluzia ako inspiracia a pomoc, pre pripad, ze sa sami neviete pohnut dalej. Prosim berte do uvahy, ze plagiatorstvo sa hodnoti 0 bodmi, neudelenim zapoctu a dalsim adekvatnym postihom podla platneho disciplinarneho radu VUT. V preklade: Skuste v prvom rade pouzit vlastnu hlavu a rozsirit svoj obzor o nove vedomosti. Koniec koncov to je jeden z dovodov preco chodime na FIT.